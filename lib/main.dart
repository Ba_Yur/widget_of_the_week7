import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double sliderValue = 50.0;
  double cupSliderValue = 50.0;
  RangeValues rangeSliderValue = RangeValues(15, 60);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Builder(
          builder: (context) {
            return Scaffold(
              appBar: AppBar(
                bottom: TabBar(
                  tabs: [
                    Tab(
                      text: 'RichText',
                    ),
                    Tab(
                      text: 'Slider',
                    ),
                    Tab(
                      text: 'Builder',
                    ),
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  Center(
                      child: RichText(
                    text: TextSpan(
                        // style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Some text',
                              style: TextStyle(color: Colors.black)),
                          TextSpan(
                              text: 'with',
                              style: TextStyle(
                                fontSize: 30,
                                backgroundColor: Colors.red,
                              )),
                          TextSpan(
                              text: 'some styling',
                              style:
                                  TextStyle(color: Colors.green, fontSize: 40)),
                        ]),
                  )),
                  Center(
                      child: Column(
                    children: [
                      CupertinoSlider(
                        min: 0,
                        max: 100.0,
                        value: cupSliderValue,
                        onChanged: (val) {
                          setState(() {
                            cupSliderValue = val;
                          });
                        },
                      ),
                      Slider(
                        value: sliderValue,
                        min: 0,
                        max: 100.0,
                        onChanged: (value) {
                          setState(() {
                            sliderValue = value;
                          });
                        },
                      ),
                      RangeSlider(
                        values: rangeSliderValue,
                        onChanged: (RangeValues newRange) {
                          setState(() {
                            rangeSliderValue = newRange;
                          });
                        },
                        min: 0,
                        max: 100.0,
                        divisions: 10,
                      ),
                    ],
                  )),
                  Center(child: Builder(
                    builder: (context) {
                      return ElevatedButton(
                        onPressed: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('You tapped the button!!!')));
                        },
                        child: Text('Do not tap me'),
                      );
                    },
                  )),
                ],
              ),
            );
          },
        ));
  }
}
